package br.edu.ifce.ppd.jogos.jogo_1.client;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JLayeredPane;

import br.edu.ifce.ppd.jogos.jogo_1.game_logic.Board;
import br.edu.ifce.ppd.jogos.jogo_1.server.ClientHandlerInterface;

public class ClientWindow extends Thread{
	private static final String PLAYER0_IMG_PATH = "assets/playerTokenBlueSmall.png";
	private static final String PLAYER1_IMG_PATH = "assets/playerTokenRedSmall.png";
	private JFrame frame;
	private JTextField textField;
	private JTextArea textChatHistory ;
	private final String IMG_PATH = "assets/tabuleiro.png";
	private Board board;
	private int playerPosition =0;
	private ImageIcon boardIcon;
	private BufferedImage boardImg;
	private JLayeredPane boardPanel;
	private BufferedImage player0Img;
	private ImageIcon player0Icon;
	private BufferedImage player1Img;
	private ImageIcon player1Icon;
	private JLabel player0Label;
	private JLabel player1Label;
	private JPanel chatPanel;
	private JButton btnStart;
	private JPanel chatHistoryPanel;
	private JButton btnSubmit;
	private JLabel boardLabel;
	private int clientId;
	static boolean loseTurn=false;
	private JButton btnRoll;
	private ClientInterface client;
	public ClientHandlerInterface server;
	boolean reRoll = false;

	public void updateChatHistory(String chatMessage) throws RemoteException{
		System.out.println(chatMessage);
		getTextChatHistory().append(chatMessage);
		getTextChatHistory().setCaretPosition(getTextChatHistory().getDocument().getLength());

	}

	public ClientWindow(ClientHandlerInterface server) throws RemoteException {
		this.board = new Board();
		this.server = server;
		client = new Client(this);
		
		//Inicia componentes do janela de jogo
		initialize();
		this.clientId = server.login(client);
		updateChatHistory("Conectado ao server.\nVocê é o jogador " +clientId+". \n");
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		chatPanel = new JPanel();
		frame.getContentPane().add(chatPanel, BorderLayout.SOUTH);

		textField = new JTextField();
		chatPanel.add(textField);
		textField.setColumns(40);

		btnSubmit = new JButton("Submit");
		btnStart = new JButton("Start");
		btnRoll = new JButton("Roll");
		btnSubmit.addActionListener(new ButtonSubmitClickListener());
		btnStart.addActionListener(new ButtonStartClickListener());
		btnRoll.addActionListener(new ButtonRollActionListener());
		chatPanel.add(btnSubmit);
		chatPanel.add(btnRoll);
		chatPanel.add(btnStart);
		btnStart.setEnabled(false);
		btnRoll.setEnabled(false);

		chatHistoryPanel = new JPanel();
		frame.getContentPane().add(chatHistoryPanel, BorderLayout.WEST);

		textChatHistory = new JTextArea();
		textChatHistory.setRows(34);
		textChatHistory.setColumns(20);
		textChatHistory.setEditable(false);
		JScrollPane scroll = new JScrollPane(textChatHistory);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		chatHistoryPanel.add(scroll);

		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt){
				disconnect(evt);
			}
		});

		boardPanel = new JLayeredPane();
		frame.getContentPane().add(boardPanel, BorderLayout.CENTER);
		boardPanel.setLayout(null);

		try {
			boardImg = ImageIO.read(new File(IMG_PATH));
			boardIcon = new ImageIcon(boardImg);

			player0Img = ImageIO.read(new File(PLAYER0_IMG_PATH));
			player0Icon = new ImageIcon(player0Img);

			player1Img = ImageIO.read(new File(PLAYER1_IMG_PATH));
			player1Icon = new ImageIcon(player1Img);

		} catch (IOException e) {
			e.printStackTrace();
		}

		boardLabel = new JLabel(boardIcon);
		player0Label = new JLabel(player0Icon);
		player1Label = new JLabel(player1Icon);

		boardPanel.add(boardLabel, 1);
		boardLabel.setBounds(0, 0, 535, 348);
		frame.setVisible(true);
	}

	protected void disconnect(WindowEvent evt) {
		try {
			server.disconnect(clientId);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		frame.dispose();
	}
	public void setPlayerTurn(int clientIdEventFrom) throws RemoteException {
		if(clientIdEventFrom==clientId){
			btnRoll.setEnabled(false);
		}else{
			btnRoll.setEnabled(true);
			updateChatHistory("É a sua vez.\nRole os dados.");
		}
	}

	public void loseTurn() throws RemoteException {
		updateChatHistory("Você ficará um turno sem jogar.\n");
		loseTurn=true;
		btnRoll.setEnabled(false);
	}

	public void gameOver(int clientIdEventFrom) throws RemoteException {
		localMoveToken(clientIdEventFrom, 34);
		if(clientIdEventFrom==clientId){
			updateChatHistory("Parabéns! Você ganhou o jogo.\n");
		}
		btnRoll.setEnabled(false);
	}

	public void startGame() throws RemoteException {
		updateChatHistory("Começando o jogo...\n");
		updateChatHistory("Decidam através do chat quem \ndeve começar a jogar. \nSomente o primeiro a jogar deve \napertar o botão de START."+"\n");

		//Adiciona jogadores ao tabuleiro e define posição inicial
		boardPanel.add(player0Label, 0);
		boardPanel.add(player1Label, 0);
		player0Label.setBounds(board.getBoardPositions()[0].getImage_x(), 
				board.getBoardPositions()[0].getImage_y(), 30, 42);
		player1Label.setBounds(board.getBoardPositions()[0].getImage_x(), 
				board.getBoardPositions()[0].getImage_y()+Board.OFFSET_PLAYER_1_y, 30, 42);
		btnStart.setEnabled(true);
	}

	public void rerollDice(int clientIdEventFrom,int newPosition) throws RemoteException{
		localMoveToken(clientIdEventFrom, newPosition);

		if(clientIdEventFrom==clientId){
			updateChatHistory("Re-role os dados.\n");
			System.out.println("ROLL ENABLE");
		}else{
			updateChatHistory("O oponente irá re-rolar os dados.\n");
			
		}
		btnRoll.setEnabled(true);
	}

	public void localMoveToken(int clientIdEventFrom,int newPosition) {
		if(clientIdEventFrom==clientId){
			try {
				player0Label.setBounds(board.getBoardPositions()[newPosition].getImage_x(), 
						board.getBoardPositions()[newPosition].getImage_y(), 30, 42);

				updateChatHistory("Você moveu para a casa "+newPosition+".\n");
				playerPosition = newPosition;
				server.handleUpdatePlayerMove(clientId,newPosition);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	public void updateTokenMove(int clientIdEventFrom, int newUpPosition) throws RemoteException{

		if(clientIdEventFrom!=clientId){
			player1Label.setBounds(board.getBoardPositions()[newUpPosition].getImage_x(), 
					board.getBoardPositions()[newUpPosition].getImage_y()+Board.OFFSET_PLAYER_1_y, 30, 42);
			updateChatHistory("Oponente moveu para a casa "+newUpPosition+".\n");
			if(newUpPosition != 34){
				btnRoll.setEnabled(true);
			}else {
				updateChatHistory("Sinto muito. Você perdeu.\n");
			}	
		}
	}
	//Método para enviar mensagens do chat  
	private class ButtonSubmitClickListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String newMessage = getTextField().getText();
			getTextField().setText("");
			try {
				server.handleChatHistory(clientId, "Jogador "+clientId+": "+newMessage);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}		
	}
	private class ButtonStartClickListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				server.handleChatHistory(clientId, "Jogador "+clientId+" irá jogar primeiro\n");
				server.handleGameBegin();
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
			
			btnRoll.setEnabled(true);
		}
	}

	private class ButtonRollActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			try {
				server.handleRollDice(loseTurn,clientId, playerPosition);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
			btnRoll.setEnabled(false);
		}
	}

	public JTextArea getTextChatHistory() {
		return textChatHistory;
	}

	public void setTextChatHistory(JTextArea textChatHistory) {
		this.textChatHistory = textChatHistory;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}

	public void disableStartButton() {
		btnStart.setEnabled(false);
	}

	public void disableLoseTurn() {
		loseTurn=false;
	}
	
	public int getClientId(){
		return clientId;
	}
}
