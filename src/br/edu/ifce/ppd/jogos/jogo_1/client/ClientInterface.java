package br.edu.ifce.ppd.jogos.jogo_1.client;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientInterface extends Remote{
	public void updateChatHistory(String message) throws RemoteException;
	public void movePlayer(int playerId, int newPosition) throws RemoteException;
	public void updatePlayerPosition(int playerId, int newPosition) throws RemoteException;
	public void rerollDice(int playerId, int newPosition) throws RemoteException;
	public void loseTurn () throws RemoteException;
	public void disableLoseTurn () throws RemoteException;
	public void setPlayerTurn (int playerId) throws RemoteException;
	public void startGame() throws RemoteException;
	public void gameOver(int winnerPlayerId) throws RemoteException;
	public void disableStartButton() throws RemoteException;
	public int getClientId() throws RemoteException;
}
