package br.edu.ifce.ppd.jogos.jogo_1.client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import br.edu.ifce.ppd.jogos.jogo_1.server.ClientHandlerInterface;

@SuppressWarnings("serial")
public class Client extends UnicastRemoteObject implements ClientInterface {
	private ClientWindow clientWindow;

	public static void main(String[] args) {
		try {
			ClientHandlerInterface server = (ClientHandlerInterface)Naming.lookup("rmi://localhost/ppd_rmi");
			ClientWindow window = new ClientWindow(server);
			Client client = new Client(window);
		} catch (RemoteException | MalformedURLException | NotBoundException e) {
			e.printStackTrace();
		}
	}
	
	public Client(ClientWindow clientWindow) throws RemoteException {
		this.clientWindow = clientWindow;
	}

	@Override
	public void movePlayer(int playerId, int newPosition)
			throws RemoteException {
		this.clientWindow.localMoveToken(playerId,newPosition);
	}

	@Override
	public void updatePlayerPosition(int playerId, int newPosition)
			throws RemoteException {
		this.clientWindow.updateTokenMove(playerId, newPosition);
	}

	@Override
	public void rerollDice(int playerId, int newPosition)
			throws RemoteException {
		clientWindow.rerollDice(playerId, newPosition);
	}

	@Override
	public void startGame() throws RemoteException {
		clientWindow.startGame();
	}

	@Override
	public void gameOver(int winnerPlayerId) throws RemoteException {
		clientWindow.gameOver(winnerPlayerId);
	}

	@Override
	public void updateChatHistory(String message) throws RemoteException {
		clientWindow.updateChatHistory(message);
	}

	@Override
	public void loseTurn() throws RemoteException {
		clientWindow.loseTurn();
	}

	@Override
	public void setPlayerTurn(int playerId) throws RemoteException {
		clientWindow.setPlayerTurn(playerId);
	}

	@Override
	public void disableStartButton() throws RemoteException {
		clientWindow.disableStartButton();
	}

	@Override
	public void disableLoseTurn() throws RemoteException {
		clientWindow.disableLoseTurn();		
	}

	@Override
	public int getClientId() throws RemoteException {
		return clientWindow.getClientId();
	}

		
}