package br.edu.ifce.ppd.jogos.jogo_1.game_logic;

public class BoardPosition {
	private TileEvent tileEvent;
	private int image_x;
	private int image_y;
	
	public BoardPosition(TileEvent tileEvent, int image_x, int image_y) {
		super();
		this.tileEvent = tileEvent;
		this.image_x = image_x;
		this.image_y = image_y;
	}
	
	public TileEvent getTileEvent() {
		return tileEvent;
	}
	public void setTileEvent(TileEvent tileEvent) {
		this.tileEvent = tileEvent;
	}
	public int getImage_x() {
		return image_x;
	}
	public void setImage_x(int image_x) {
		this.image_x = image_x;
	}
	public int getImage_y() {
		return image_y;
	}
	public void setImage_y(int image_y) {
		this.image_y = image_y;
	}
}
