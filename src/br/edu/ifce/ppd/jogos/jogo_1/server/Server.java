package br.edu.ifce.ppd.jogos.jogo_1.server;

import java.net.MalformedURLException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.*;

public class Server {
	public static void main(String[] args) {
		try {
			//Registry registry = LocateRegistry.createRegistry(1099);
			ClientHandlerInterface clientHandler = new ClientHandler();
			Naming.rebind("rmi://localhost/ppd_rmi", clientHandler);
			System.out.println("Server iniciado!");
		} catch (RemoteException | MalformedURLException e) {
			e.printStackTrace();
		}
	}
}
