package br.edu.ifce.ppd.jogos.jogo_1.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import br.edu.ifce.ppd.jogos.jogo_1.client.ClientInterface;

public interface ClientHandlerInterface extends Remote{
	//TODO: remover handleEvent
	public int login(ClientInterface client) throws RemoteException;
	public void disconnect(int clientId)  throws RemoteException;
	public void handleChatHistory(int clientId, String message) throws RemoteException;
	public void handleRollDice(boolean loseTurn,int clientId, int oldPlayerPosition) throws RemoteException;
	public void handleGameStart() throws RemoteException;
	public void handleGameOver(int winnerClientId) throws RemoteException;
	public void handleUpdatePlayerMove(int clientId, int newPosition ) throws RemoteException;
	public void handleGameBegin() throws RemoteException;
}
