package br.edu.ifce.ppd.jogos.jogo_1.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;

import br.edu.ifce.ppd.jogos.jogo_1.client.ClientInterface;
import br.edu.ifce.ppd.jogos.jogo_1.game_logic.Board;
import br.edu.ifce.ppd.jogos.jogo_1.game_logic.TileEvent;
import br.edu.ifce.ppd.jogos.jogo_1.game_logic.Utils;


public class ClientHandler extends UnicastRemoteObject implements ClientHandlerInterface{
	//Bus handle only server-side events locally. All events between server and clients are done with Sockets.
	private Server server;
	private Board board;
	private int id;
	private Vector<ClientInterface> clientList;

	public ClientHandler() throws RemoteException{
		this.board = new Board();
		this.id=-1;
		clientList = new Vector<ClientInterface>();
	}

	@Override
	public void disconnect(int clientId) throws RemoteException{
		for (int i=0; i<clientList.size();i++) {
			if(clientId==clientList.get(i).getClientId()){
				clientList.remove(i);
			}
		}
		handleChatHistory(clientId, "Jogador "+clientId+ " desconectou.");
		id--;
	}

	@Override
	public int login(ClientInterface client) throws RemoteException {
		id++;
		if(clientList.size()>=2){
			client.updateChatHistory("Impossível conectar. Tente novamente depois.\n");
			id= -1;
		} else {
			clientList.add(client);
			if(clientList.size()==2) {
				for (ClientInterface c : clientList) {
					c.startGame();
				}
			}
		}
		return id;
	}

	@Override
	public void handleRollDice(boolean loseTurn,int clientIdEventFrom, int oldPlayerPosition) throws RemoteException {
		System.out.println("PLAYER "+clientIdEventFrom + " ROLL ");

		if(loseTurn){
			System.out.println("LOSE TURN CASE");
			clientList.get(clientIdEventFrom).updateChatHistory("Jogador "+clientIdEventFrom+" não jogará este turno.\n");
			clientList.get(clientIdEventFrom).disableLoseTurn();

			for (ClientInterface client : clientList) {
				if(client.getClientId()!=clientIdEventFrom) client.setPlayerTurn(clientIdEventFrom);
			}
		} else {
			clientList.get(clientIdEventFrom).setPlayerTurn(clientIdEventFrom);

			int result = Utils.randInt(1, 6);
			handleChatHistory(clientIdEventFrom, "Jogador "+clientIdEventFrom+" tirou "+result+" nos dados.\n");

			int newPlayerPosition = result+oldPlayerPosition; 

			//End of the Board condition
			if(newPlayerPosition>=34){
				newPlayerPosition=34;
			}
			handleTileEvent(clientIdEventFrom, newPlayerPosition);
		}
	}

	private void handleTileEvent(int clientId, int newPlayerPosition) throws RemoteException {
		TileEvent tileEvent = board.getBoardPositions()[newPlayerPosition].getTileEvent();
		System.out.println("Player "+clientId+" ROLL - Tile Event = "+tileEvent.toString());
				
		switch (tileEvent) {
		case END:
			//Broadcast game over to clients
			for (ClientInterface clientInterface : clientList) {
				clientInterface.gameOver(clientId);
			}
			break;
		case GO_BACK:
			handleChatHistory(clientId,"Jogador "+clientId+" caiu na casa de voltar duas casas.\n");
			newPlayerPosition= newPlayerPosition -2;
			for (ClientInterface clientInterface : clientList) {
				clientInterface.movePlayer(clientId,newPlayerPosition);
			} 
			break;
		case GO_FORWARD:
			handleChatHistory(clientId,"Jogador "+clientId+" caiu na casa de avançar duas casas.\n");
			newPlayerPosition= newPlayerPosition +2;
			for (ClientInterface clientInterface : clientList) {
				clientInterface.movePlayer(clientId,newPlayerPosition);
			}
			break;
		case LOSE_TURN:
			handleChatHistory(clientId,"Jogador "+clientId+" caiu na casa de perder um turno.");
			clientList.get(clientId).movePlayer(clientId,newPlayerPosition);
			clientList.get(clientId).loseTurn();
			System.out.println("Client "+ clientId + " will lose turn ");
			break;
		case NOP:
			//reply with MOVE only
			clientList.get(clientId).movePlayer(clientId,newPlayerPosition);
			break;
		case ROLL_AGAIN:
			handleChatHistory(clientId,"Jogador "+clientId+" caiu na casa re-rolar os dados.\n");
			for (ClientInterface clientInterface : clientList) {
				clientInterface.rerollDice(clientId,newPlayerPosition);
				/*if(clientInterface.getClientId()!=clientId){
					clientInterface.setPlayerTurn(clientId);
				}*/
			}

			break;
		default:
			break;
		}
	}

	@Override
	public void handleChatHistory(int clientId, String message) throws RemoteException{
		for (ClientInterface clientInterface : clientList) {
			clientInterface.updateChatHistory(message);
		}
	}

	@Override
	public void handleGameStart() throws RemoteException {
		for (ClientInterface clientInterface : clientList) {
			clientInterface.startGame();
		}
	}

	@Override
	public void handleGameOver(int winnerClientId) throws RemoteException {
		for (ClientInterface clientInterface : clientList) {
			clientInterface.gameOver(winnerClientId);
		}
	}

	@Override
	public void handleUpdatePlayerMove(int clientId, int newPosition)
			throws RemoteException {
		for (ClientInterface clientInterface : clientList) {
			clientInterface.updatePlayerPosition(clientId, newPosition);
		}
	}

	@Override
	public void handleGameBegin() throws RemoteException {
		for (ClientInterface clientInterface : clientList) {
			clientInterface.disableStartButton();
		}
	}
	
}
